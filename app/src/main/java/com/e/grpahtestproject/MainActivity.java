package com.e.grpahtestproject;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;

import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    ArrayList<SymptomsVitalsModel> modelsList=new ArrayList<>();
    final ArrayList<String> dateDisplayList = new ArrayList<>();
    final ArrayList<String> dateValueList = new ArrayList<>();
    LineChart lc_graph;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lc_graph=findViewById(R.id.lc_graph);

        modelsList.add(new SymptomsVitalsModel("102.0","2020-07-07","11:18:15","2020-07-07 11:18:15"));
        modelsList.add(new SymptomsVitalsModel("105.0","2020-07-07","12:28:26","2020-07-07 12:28:26"));
        modelsList.add(new SymptomsVitalsModel("99.0","2020-07-07","14:35:528","2020-07-07 1435:28"));
        modelsList.add(new SymptomsVitalsModel("96.7","2020-07-08","14:44:56","2020-07-08 14:44:56"));
        modelsList.add(new SymptomsVitalsModel("104.0","2020-07-08","15:28:57","2020-07-08 15:28:57"));
        modelsList.add(new SymptomsVitalsModel("95.0","2020-07-09","11:18:15","2020-07-09 11:18:15"));
        modelsList.add(new SymptomsVitalsModel("100.0","2020-07-09","11:28:26","2020-07-09 11:28:26"));
        modelsList.add(new SymptomsVitalsModel("98.0","2020-07-09","11:35:528","2020-07-09 11:35:28"));

        dateDisplayList.add("07 July");
        dateDisplayList.add("08 July");
        dateDisplayList.add("09 July");


        dateValueList.add("2020-07-07");
        dateValueList.add("2020-07-08");
        dateValueList.add("2020-07-09");

            setGraph();
    }

    public void setGraph()
    {

        ArrayList<ILineDataSet> dataSetArrayList = new ArrayList<>();
            List<Entry> entries = new ArrayList<Entry>();
            for (int k = 0; k < dateValueList.size(); k++) {
                for (int j = 0; j < modelsList.size(); j++) {
                    if (dateValueList.get(k).equalsIgnoreCase(modelsList.get(j).getDate())) {
                        entries.add(new BarEntry(k, Float.parseFloat(modelsList.get(j).getValue())));
                    }
                }
            }
            LineDataSet dataSet = new LineDataSet(entries, "");
            dataSet.setLineWidth(1f);
            dataSet.setCircleRadius(3f);
            dataSet.setDrawValues(false);
            dataSet.setHighLightColor(Color.TRANSPARENT);// highlight need to enable to show marker
            dataSet.setCircleHoleColor(getResources().getColor(R.color.white_color));
            dataSet.setColors(getResources().getColor(R.color.black));
            dataSet.setCircleColor(getResources().getColor(R.color.black));
            dataSetArrayList.add(dataSet);
        LineData data1 = new LineData(dataSetArrayList);
        lc_graph.setData(data1);
        lc_graph.getDescription().setEnabled(false);
        lc_graph.setScaleYEnabled(false);
        lc_graph.getLegend().setEnabled(false);
        lc_graph.setDoubleTapToZoomEnabled(false);
        YAxis yAxis1 = lc_graph.getAxisLeft();
        lc_graph.getAxisLeft().setDrawGridLines(false);  //left
        lc_graph.getAxisRight().setDrawGridLines(false);   //right
        lc_graph.getAxisRight().setEnabled(false);
        yAxis1.setTextSize(10f); // set the text size
        yAxis1.setAxisMinimum(95f); // start at zero
        yAxis1.setAxisMaximum(105f); // the axis maximum is 100
        yAxis1.setGranularity(1f); // interval 1
        yAxis1.setLabelCount(11);

        XAxis xAxis = lc_graph.getXAxis();
        xAxis.setPosition(XAxis.XAxisPosition.BOTTOM);
        xAxis.setTextSize(10f); // set the text size
        xAxis.setGranularity(1f);
        xAxis.setDrawGridLines(false);
        xAxis.setValueFormatter(new IndexAxisValueFormatter(dateDisplayList));
        xAxis.setAxisMinimum(-0.1f);
        xAxis.setAxisMaximum(dateDisplayList.size());
        lc_graph.invalidate();

    }

}
