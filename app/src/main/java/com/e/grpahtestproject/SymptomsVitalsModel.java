package com.e.grpahtestproject;

import java.util.List;

public class SymptomsVitalsModel {
            /**
             * value : 4
             * date : 2020-02-24
             * time : 16:20:59
             * created_at : 2020-02-24 16:20:59
             */

            private String value;
            private String date;
            private String time;
            private String created_at;


    public SymptomsVitalsModel(String value, String date, String time, String created_at) {
        this.value = value;
        this.date = date;
        this.time = time;
        this.created_at = created_at;
    }

    public String getValue() {
                return value;
            }

            public void setValue(String value) {
                this.value = value;
            }

            public String getDate() {
                return date;
            }

            public void setDate(String date) {
                this.date = date;
            }

            public String getTime() {
                return time;
            }

            public void setTime(String time) {
                this.time = time;
            }

            public String getCreated_at() {
                return created_at;
            }

            public void setCreated_at(String created_at) {
                this.created_at = created_at;
            }
}
